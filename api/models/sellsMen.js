const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SellsMenSchema = mongoose.Schema({
   
    name: {
        type: String,
        require:true,
        unique:1
    },
    item:{
      type:String  
    },
    contactNumber:{
        type:String,
    },
    ammount:{
        type:Number,
        default:0
        
    },
    ammountDetails:[{
        type:Schema.Types.ObjectId,
        ref: 'sellsMenDetail'
    }]
    
},
{ timestamps: true }
)

const SellsMen = mongoose.model('sellsMen', SellsMenSchema);

module.exports = SellsMen;   