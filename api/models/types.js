const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const typesSchema = mongoose.Schema({

    name: {
        type: String,
        unique: 1,
        require: true
    },

    price: {
        type: Array,
        require: true
    },
    count: {
        totalCount: {type:Number},
        particular:{type:String},
        addition:{type:Number}
    },
    itemId:{
        type:Schema.Types.ObjectId,
        ref: 'item'
    },
    particulars:[{
        type:Schema.Types.ObjectId,
        ref:'particular',
    }],
    shortKey:{
        type:Number,
    }

},
    { timestamps: true }
)

const Types = mongoose.model('type', typesSchema);

module.exports = Types;