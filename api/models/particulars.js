const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const particularsSchema = mongoose.Schema({
   
    type: {
        type: String,
        require:true,
        unique:1
    },

    price:[{
        type:Number,
        require:true
    }],
    // itemType:[{
    //     type:Schema.Types.ObjectId,
    //     ref:'type',
    // }]


},
{ timestamps: true }
)

const Particulars= mongoose.model('particular', particularsSchema);

module.exports = Particulars;