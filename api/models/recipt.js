const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const reciptSchema = mongoose.Schema({

    clientName: {
        type: String,
        require: true,
    },
    totalAmmount: {
        type: Number
    },
    serialNumber: {
        type: Number
    },
    date: { type: String },
    recipt: {
        type: Array
    },
    pending: {
        type: Number
    },
    paid: {
        type: Number
    },
    deductionArray: {
        type: Array
    },
    isPendingPrint: {
        type: Boolean
    }

},
    { timestamps: true }
)

const Recipt = mongoose.model('recipt', reciptSchema);

module.exports = Recipt;