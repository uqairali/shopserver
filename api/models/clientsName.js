const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ClientsNameSchema = mongoose.Schema({
   
    name: {
        type: String,
        require:true,
        unique:1
    },
    address:{
      type:String  
    },
    contactNumber:{
        type:String,
    },
    
},
{ timestamps: true }
)

const ClientsName = mongoose.model('clientsName', ClientsNameSchema);

module.exports = ClientsName;   