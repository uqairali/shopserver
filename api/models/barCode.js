const mongoose = require('mongoose');
const barCodeSchema = mongoose.Schema({
   
    barCode: {
        type: String,
        unique: 1,
    },
    name:{
    type:String
    },
    price:{
   type:Number
    },
    
},
{ timestamps: true }
)

const BarCode = mongoose.model('barCode', barCodeSchema);

module.exports = BarCode;


