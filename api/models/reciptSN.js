const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const reciptSNSchema = mongoose.Schema({
   
    serialNumber: {
        type: Number,
        default:1
    },
},
{ timestamps: true }
)

const ReciptSN = mongoose.model('reciptSN', reciptSNSchema);

module.exports = ReciptSN;