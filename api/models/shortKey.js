const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const shortKeySchema = mongoose.Schema({
   
    key: {
        type: Number,
        require:true,
        unique:1
    },
    itemTypeId:{
        require:true,
        type:Schema.Types.ObjectId,
        ref: 'type'
   },

},
{ timestamps: true }
)

const ShortKey = mongoose.model('shortKey', shortKeySchema);

module.exports = ShortKey;