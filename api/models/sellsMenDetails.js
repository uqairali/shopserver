const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SellsMenDetailsSchema = mongoose.Schema({

    payment: {
        type: Number,
        require: true
    },
    isPayed: {
        type: Boolean,
        require: true
    },
    discription: {
        type: String,
    },
    date: {
        type: String,
        require: true
    },
    name: {
        type: Schema.Types.ObjectId,
        ref: 'sellsMen'
    }

},
    { timestamps: true }
)

const SellsMenDetails = mongoose.model('sellsMenDetail', SellsMenDetailsSchema);

module.exports = SellsMenDetails;   