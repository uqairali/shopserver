const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const itemSchema = mongoose.Schema({
   
    name: {
        type: String,
        require:true,
        unique:1
    },
   particulars:{
       type:Array
   },
    types:[{
        type:Schema.Types.ObjectId,
        ref: 'type'
    }]
},
{ timestamps: true }
)

const Items = mongoose.model('item', itemSchema);

module.exports = Items;