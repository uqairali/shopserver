const Items = require('../models/items');
const Types = require('../models/types')

exports.addTypes = async (req, res) => {
  var pushVal="types"
  try {
    var types = new Types(req.body)
    var saveType = await types.save()
    var updateItem = await Items.findByIdAndUpdate({ _id:req.body.itemId},{$push:{[pushVal]: saveType._id }},{new:true})
    res.status(200).json(updateItem)
  } catch (err) {
    res.status(400).json(err)
  }
}
exports.getTypes = async (req, res) => {
  try {
    var doc = await Types.find()
    .populate({
      path: 'itemId',
      match: {
        //active: true
      }
    })
    .populate({
      path: 'particulars',
      match: {
        //active: true
      }
    })
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}
exports.deleteType = async (req, res) => {
  var popFiled="types"
  console.log(req.params.typeId,req.params.itemId)
  try {
    const doc = await Types.findOneAndDelete({ _id: req.params.typeId })
    await Items.findOneAndUpdate({_id:req.params.itemId},{$pull:{[popFiled]:req.params.typeId}})
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}
exports.updateType=async(req,res)=>{
  try{
   const doc=await Types.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true })
   res.status(200).json(doc)
  }catch(err){
    res.status(400).json(err)
  }
}
exports.updateitemTotalCount=async(req,res)=>{
 
  var i=0
  for(i;i<=req.body.length-1;i++){
    try{
      const doc=await Types.findByIdAndUpdate({ _id: req.body[i].typeId }, {count:req.body[i].count}, { new: true })
      if(req.body.length===i+1)
      res.status(200).json("suuccess")
    }catch(err){
      i=req.body.length;
      res.status(400).json(err)
    }
  }
}
