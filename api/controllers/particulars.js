const Particular = require('../models/particulars')
const Type=require('../models/types')

exports.addParticular = async (req, res) => {
  var pushVal="particulars"
  try {
    var particular = new Particular(req.body)
    var saveParticular = await particular.save()
    var updateType = await Type.findByIdAndUpdate({ _id:req.body.typeId},{$push:{[pushVal]: saveParticular._id }},{new:true})
    res.status(200).json(updateType)
  } catch (err) {
    res.status(400).json(err)
  }
}