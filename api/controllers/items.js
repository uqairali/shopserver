const Items = require('../models/items');

exports.saveItem=(req,res)=>{
  console.log(req.body)
const items=new Items(req.body)
items.save((err,doc)=>{
  if(err)
  res.status(400).json(err)
  else
  res.status(200).json(doc)
})
}

exports.getAllItems=async(req,res)=>{
  try{
   const doc=await Items.find().populate({
    path: 'types',
    match: {
      //active: true
    },
    populate: {
      path: 'itemId',
    } 
  })
   res.status(200).json(doc)
  }catch(err){
    res.status(400).json(err)
  }
}


exports.updateItem=async(req,res)=>{
  try{
   const doc=await Items.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true })
   res.status(200).json(doc)
  }catch(err){
    res.status(400).json(err)
  }
}

exports.deleteItem=async(req,res)=>{
  try{
   const doc=await Items.findOneAndDelete({ _id: req.params.id })
   res.status(200).json(doc)
  }catch(err){
    res.status(400).json(err)
  }
}