const Clients = require('../models/clientsName');

exports.newClient = async (req, res) => {
    try {

            const client = new Clients(req.body)
            var doc = await client.save()
            res.status(200).json(doc)
       
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.allClientsName = async (req, res) => {
    try {
        var data = await Clients.find()
        res.status(200).json(data)
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.updateClientName=async(req,res)=>{
    try{
     const doc=await Clients.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true })
     res.status(200).json(doc)
    }catch(err){
      res.status(400).json(err)
    }
  }
exports.deleteClientName=async(req,res)=>{
    try{
     const doc=await Clients.findOneAndDelete({ _id: req.params.id })
     res.status(200).json(doc)
    }catch(err){
      res.status(400).json(err)
    }
  }