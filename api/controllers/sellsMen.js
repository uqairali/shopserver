const SellsMen = require('../models/sellsMen');
const SellsMenDetails=require('../models/sellsMenDetails')
exports.newSellsMen = async (req, res) => {
    try {

            const sellsMen = new SellsMen(req.body)
            var doc = await sellsMen.save()
            res.status(200).json(doc)
       
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.allSellsMen = async (req, res) => {
    try {
        var data = await SellsMen.find().populate({
          path: 'ammountDetails',
          match: {
            //active: true
          },
        })
        res.status(200).json(data)
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.updateSellsMen=async(req,res)=>{
  console.log("uqair")
    try{
     const doc=await SellsMen.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true })
     res.status(200).json(doc)
    }catch(err){
      res.status(400).json(err)
    }
  }
exports.deleteSellsMen=async(req,res)=>{
    try{
     const doc=await SellsMen.findOneAndDelete({ _id: req.params.id })
     await SellsMenDetails.deleteMany({name:req.params.id})
     res.status(200).json(doc)
    }catch(err){
      res.status(400).json(err)
    }
  }

  exports.allAmmount = async (req, res) => {
    try {
        var data = await SellsMen.find().select("ammount")
        res.status(200).json(data)
    } catch (err) {
        res.status(400).json(err)
    }
}