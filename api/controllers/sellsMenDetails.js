const SellsMenDetails = require('../models/sellsMenDetails');
const SellsMen=require('../models/sellsMen')
exports.newSellsMenDetails = async (req, res) => {
  var pushVal="ammountDetails"
  var payment=0;
  var val=+req.body.payment
  if(req.body.isPayed){
    payment=-val
  }else{
    payment=val
  }
  console.log(payment)
    try {
            const sellsMenDetails = new SellsMenDetails(req.body)
            var doc = await sellsMenDetails.save()
            var updateSellsMen = await SellsMen.findByIdAndUpdate({ _id:req.body.name},{$push:{[pushVal]: doc._id },$inc: { ammount:payment }},{new:true})
            res.status(200).json(doc)
       
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.allSellsMenDetails = async (req, res) => {
  var date=req.params.date
  console.log(date)
  try {
      const doc = await SellsMenDetails.aggregate(
        [
          { $match: {date} },
  
        ]);
        await SellsMenDetails.populate(doc,'name');
        res.status(200).json(doc)
    } catch (err) {
        res.status(400).json(err)
    }
}

exports.deleteSellsMenDetails=async(req,res)=>{
    try{
      var pushVal="ammountDetails"
      var payment=0;
      var val=+req.params.payment
      if(req.params.isPayed==='true'){
        payment=val
      }else{
        payment=-val
      }
     const doc=await SellsMenDetails.findOneAndDelete({ _id: req.params.id })
      await SellsMen.findByIdAndUpdate({ _id:req.params.sellsMenId},{$pull:{[pushVal]: req.params.id },$inc: { ammount:payment }},{new:true})
     res.status(200).json(doc)
    }catch(err){
      res.status(400).json(err)
    }
  }

  exports.findDetailsById = async (req, res) => {
    console.log(req.params.id)
    try {
        var data = await SellsMenDetails.find({name:req.params.id}).sort('date').populate({
          path: 'name',
          match: {
            //active: true
          },
        }).sort({createdAt:-1})
        res.status(200).json(data)
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.updateDetails=async(req,res)=>{
  console.log(req.params.preVal,req.params.sellsMenId,req.body)
  try{
    var payment=0;
      var preVal=0
      var val=+req.body.payment
      var oldVal=+req.params.preVal
      if(req.body.isPayed){
        payment=-val
       preVal=oldVal
      }else{
        payment=val
        preVal=-oldVal
      }
      console.log(payment,preVal,req.body.isPayed)
      var data=await SellsMenDetails.findByIdAndUpdate({_id:req.params.id}, req.body, { new: true })
      await SellsMen.findByIdAndUpdate({ _id:req.params.sellsMenId},{$inc: { ammount:preVal }})
      await SellsMen.findByIdAndUpdate({ _id:req.params.sellsMenId},{$inc: { ammount:payment }})
      res.status(200).json(data)
  }catch(err){
    res.status(400).json(err)
  }
}