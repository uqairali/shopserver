const ShortKey = require('../models/shortKey');

exports.saveNewShortKey=(req,res)=>{
const shortKey=new ShortKey(req.body)
 shortKey.save((err,doc)=>{
  if(err)
  res.status(400).json(err)
  else
  res.status(200).json(doc)
})
}

exports.allShortKeys=async(req,res)=>{
  try{
   const doc=await ShortKey.find().populate({
    path: 'itemTypeId',
    match: {
      //active: true
    },
    populate: {
      path: 'itemId',
    } 
  })
   res.status(200).json(doc)
  }catch(err){
    res.status(400).json(err)
  }
}

exports.deleteShorKey=async(req,res)=>{
  try{
   const doc=await ShortKey.findOneAndDelete({ _id: req.params.id })
   res.status(200).json(doc)
  }catch(err){
    res.status(400).json(err)
  }
}
exports.updateShortKey=async(req,res)=>{
  try{
   const doc=await ShortKey.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true })
   res.status(200).json(doc)
  }catch(err){
    res.status(400).json(err)
  }
}
exports.getByKey=async(req,res)=>{
  try{
   const doc=await ShortKey.findOne({ key: req.params.key })
   res.status(200).json(doc)
  }catch(err){
    res.status(400).json(err)
  }
}