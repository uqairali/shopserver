const BarCode = require('../models/barCode');

exports.getBarCode = async (req, res) => {
    try {
        var data = await BarCode.findOne({ barCode: req.params.barCode })
        res.status(200).json(data)
    } catch (err) {
        res.status(400).json(err)
    }
}
exports.newBarCode = async (req, res) => {
    try {
        const barCode = new BarCode(req.body)
        var doc = await barCode.save()
        res.status(200).json(doc)

    } catch (err) {
        res.status(400).json(err)
    }
}
exports.updateBarCode = async (req, res) => {
    try {
        const barCode =await BarCode.findByIdAndUpdate({_id:req.params.id},req.body)
        res.status(200).json(barCode)

    } catch (err) {
        res.status(400).json(err)
    }
}