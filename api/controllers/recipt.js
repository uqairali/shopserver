const Recipt = require('../models/recipt');
const { getDateFromParam } = require('../../utils/helper')

exports.saveRecipt = (req, res) => {
  let date = req.params.date ? getDateFromParam(req.params.date).toDateString() : new Date().toDateString(); //new Date('01/26/2020')
  req.body.date = date
  const recipt = new Recipt(req.body)
  recipt.save((err, doc) => {
    if (err)
      res.status(400).json(err)
    else
      res.status(200).json(doc)
  })
}

exports.getRecipt = async (req, res) => {
  let date = req.params.date ? getDateFromParam(req.params.date).toDateString() : new Date().toDateString(); //new Date('01/26/2020')
  if (date == 'Invalid Date') {
    res.status(401).send({ message: "Please provide valid date e.g. 01-24-2020 " })
  }
  try {
    const doc = await Recipt.aggregate(
      [
        { $match: { date } },
        { $sort: { createdAt: -1 } }

      ]);
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}

exports.getAllrecipts = async (req, res) => {
  try {
    const doc = await Recipt.find()
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}
exports.updateRecipt = async (req, res) => {
  try {
    const doc = await Recipt.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true })
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}

exports.deleteRecipt = async (req, res) => {
  try {
    const doc = await Recipt.findOneAndDelete({ _id: req.params.id })
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}
exports.getReciptById = async (req, res) => {
  try {
    const doc = await Recipt.findOne({ _id: req.params.id })
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}
exports.getReciptByName = async (req, res) => {
  try {
    const docs = await Recipt.find({ clientName: req.params.name }).sort({ createdAt: -1 })
    res.status(200).json(docs)
  } catch (err) {
    res.status(400).json(err)
  }
}
exports.getReciptBySerialNumber = async (req, res) => {
  var serial = +req.params.serialnumber
  console.log(serial)
  try {
    const docs = await Recipt.findOne({ serialNumber: serial })
    res.status(200).json(docs)
  } catch (err) {
    res.status(400).json(err)
  }
}
exports.getReciptByItemTypeId = async (req, res) => {
  try {
    console.log(req.params.id)
    const docs = await Recipt.aggregate([
      {
        $match: {
          recipt: {
            $elemMatch: {
              $and: [
                { typeId: req.params.id }
              ]
            }
          }
        }
      },
      { $unwind: '$recipt' },
      { $match: { 'recipt.typeId': req.params.id } },
      { $group: { _id: '$_id', recipt: { $push: { recipt: '$recipt', date: '$date' } } } }
    ])
    res.status(200).json(docs)
  } catch (err) {
    res.status(400).json(err)
  }
}

exports.getPendingRecipts = async (req, res) => {
  try {
    const doc = await Recipt.find({ isPendingPrint: true })
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}

exports.getReciptsByMonth = async (req, res) => {
  let date = req.params.date ? getDateFromParam(req.params.date).toDateString() : new Date().toDateString(); //new Date('01/26/2020')
  if (date == 'Invalid Date') {
    res.status(401).send({ message: "Please provide valid date e.g. 01-24-2020 " })
  }
  try {
    const doc = await Recipt.find();
    res.status(200).json(doc)
  } catch (err) {
    res.status(400).json(err)
  }
}