const express = require('express');
const ItemsController = require('./controllers/items')
const TypesController = require('./controllers/types')
const ParticularController = require('./controllers/particulars')
const apiRoutes = express.Router();
const auth = require('../config/auth')
const AgentController = require('./controllers/agent')
const ReciptController = require('./controllers/recipt')
const ReciptSNController = require('./controllers/reciptSN')
const ClientsNameController = require('./controllers/clientsName')
const ShortKeyController = require('./controllers/shortKey')
const SellsMenController = require('./controllers/sellsMen')
const SellsMenDetailsController = require('./controllers/sellsMenDetails')
const BarCodeContoller=require('./controllers/barCode')

const accountSid = 'ACf5f0713e958964213877a0ffad59d700';
const authToken = 'b9de8e0e5ebfee4089f6c39df03fa79b';
const client = require('twilio')(accountSid, authToken);
apiRoutes.post('/auth/login', auth.optional, AgentController.login)
apiRoutes.post('/auth/register', auth.optional, AgentController.register)
apiRoutes.post('/item', ItemsController.saveItem)
apiRoutes.get('/item', ItemsController.getAllItems)
apiRoutes.put('/item/:id', ItemsController.updateItem)
apiRoutes.delete('/item/:id', ItemsController.deleteItem)

apiRoutes.post('/types', TypesController.addTypes)
apiRoutes.get('/types', TypesController.getTypes)
apiRoutes.delete('/types/:typeId/:itemId', TypesController.deleteType)
apiRoutes.put('/types/:id', TypesController.updateType)
apiRoutes.put('/typeTotalCount', TypesController.updateitemTotalCount)

apiRoutes.post('/particular', ParticularController.addParticular)

apiRoutes.post('/recipt', ReciptController.saveRecipt)
apiRoutes.get('/recipt/:date?', ReciptController.getRecipt)
apiRoutes.put('/recipt/:id', ReciptController.updateRecipt)
apiRoutes.delete('/recipt/:id', ReciptController.deleteRecipt)
apiRoutes.get('/reciptById/:id', ReciptController.getReciptById)
apiRoutes.get('/getReciptByItemId/:id', ReciptController.getReciptByItemTypeId)
apiRoutes.get('/getPendingRecipts', ReciptController.getPendingRecipts)
apiRoutes.get('/reciptGetAll',ReciptController.getAllrecipts)

apiRoutes.get('/incrementSN', ReciptSNController.incrementSN)
apiRoutes.get('/getSerialNumber', ReciptSNController.getReciptSN)

apiRoutes.post('/clientName/', ClientsNameController.newClient)
apiRoutes.get('/allClientsName', ClientsNameController.allClientsName)
apiRoutes.delete('/clientName/:id', ClientsNameController.deleteClientName)
apiRoutes.put('/clientName/:id', ClientsNameController.updateClientName)
apiRoutes.get('/reciptsbyname/:name', ReciptController.getReciptByName)
apiRoutes.get('/reciptbyserialnumber/:serialnumber', ReciptController.getReciptBySerialNumber)

apiRoutes.post('/newShortKey', ShortKeyController.saveNewShortKey)
apiRoutes.get('/allShortkeys', ShortKeyController.allShortKeys)
apiRoutes.put('/updateShortKey/:id', ShortKeyController.updateShortKey)
apiRoutes.delete('/deleteShortkey/:id', ShortKeyController.deleteShorKey)
apiRoutes.get('/getByKey/:key', ShortKeyController.getByKey)

apiRoutes.post('/newSellsMen', SellsMenController.newSellsMen)
apiRoutes.get('/allSellsMen', SellsMenController.allSellsMen)
apiRoutes.put('/updateSellsMen/:id', SellsMenController.updateSellsMen)
apiRoutes.delete('/deleteSellsMen/:id', SellsMenController.deleteSellsMen)
apiRoutes.get('/getAllAmmount', SellsMenController.allAmmount)

apiRoutes.post('/newSellsMenDetails', SellsMenDetailsController.newSellsMenDetails)
apiRoutes.get('/sellsMenDetails/:date', SellsMenDetailsController.allSellsMenDetails)
apiRoutes.delete('/deleteSellsMendetails/:id/:payment/:isPayed/:sellsMenId', SellsMenDetailsController.deleteSellsMenDetails)
apiRoutes.get('/getById/:id', SellsMenDetailsController.findDetailsById)
apiRoutes.put('/updateSellsMenDetails/:id/:preVal/:sellsMenId',SellsMenDetailsController.updateDetails)

apiRoutes.get('/getBarCode/:barCode',BarCodeContoller.getBarCode)
apiRoutes.post('/newBarCode',BarCodeContoller.newBarCode)
apiRoutes.put('/updateBarCode/:id',BarCodeContoller.updateBarCode)

apiRoutes.get('/test', auth.required, (req, res) => {
    res.status(200).json("server is running")
})
apiRoutes.post('/sendMessage/:number',async (req, res) => {
    try{
    var myNumber = `+92${req.params.number}`;
    myNumber.toString()
    console.log(myNumber)
    var doc=await client.messages.create({
            from: "+12566000769",
            to: myNumber,
            body: req.body.data
        })
        res.status(200).json(doc)
    }catch(err){
        res.status(400).json(err)
    }
})
module.exports = apiRoutes;