const express = require('express');

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');

require('./api/models/agents')
require('./config/passport')
const apiRoutes = require("./api/api");
const app = express();
const mongoose = require('mongoose');
require('dotenv').config();



//mongoose connections
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DATABASE, ((err,db) => {
    if (err) {
        console.log(process.env.DATABASE)
        console.log("mongodb connection failed!!!!")
    } else {
        console.log("mongodb connected success")
    }
}))

app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

app.use(cookieParser());
app.use(cors());
app.use('/api', apiRoutes);

const port = process.env.PORT || 3002;
app.listen(port, () => {
    console.log(`server is running on port ${port}`)
})