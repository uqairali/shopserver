exports.deleteWallpapersByCategory=(findVal,doc)=>{
  var newDoc=[];
  doc.map(item=>{
    var filteredAry = item.category.filter(function(e) { return e !== findVal })
     newDoc.push({category:filteredAry})
  })
  return newDoc
}
exports.getDateFromParam= (date)=>{
  return new Date(date.replace(/-/g,' '));
}